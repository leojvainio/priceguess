import logo from './logo.svg';
import './App.css';
import {Button,Col} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import PriceGuess from './Components/PriceGuess'
import PriceGuessComponent from './Components/PriceGuessComponent';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>VILKEN ÄR DYRAST?</h1>
        <PriceGuessComponent/>
      </header>
    </div>
  );
}

export default App;
