import React, { Component } from 'react';
import {Button,Col} from 'react-bootstrap'

class PriceGuessComponent extends Component {

    constructor(props){
        super(props)

        this.state = {
            message: 'Henlo'
        }
    }


    render() {
        return (
            <div>
                <div class="row">
                    <div className="LeftCar">
                        <img src="https://picsum.photos/500/300"/>
                        <p>Volvo V70 740hk Bötat</p>
                        <Button >Denna</Button>
                    </div>
                    <div className="RightCar">
                        <img src="https://picsum.photos/500/301"/>
                        <p>Volvo S60 2.4 Momentum 140hk NYBESIKTIGAD</p>
                        <Button>Denna</Button>
                    </div>
                </div>               
            </div>
        );
    }
}

export default PriceGuessComponent;